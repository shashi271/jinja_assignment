from time import strftime
from flask import Flask, render_template
from datetime import datetime
app = Flask(__name__)

@app.route("/home")
def home():
    date = datetime.now()
   
    date1 = (date.strftime("%d"))
    if(date1=="01"):
        date1 = date1+"st"
    elif(date1=="02"):
        date1 = date1+"nd"
    elif(date1=="03"):
        date1 = date1+"rd"
    else:
        date1= date1+"th"

    month = (date.strftime("%m"))
    year = (date.strftime("%y"))
    months={
        "01": "Jan",
        "02":"Feb",
        "03":"Mar",
        "04":"Apr",
        "05":"May",
        "06":"Jun",
        "07":"Jul",
        "08":"Aug",
        "09":"Sep",
        "10":"Oct",
        "11":"Nov",
        "12":"Dec"
        
    }
    final = date1 + " " + months[month] + " " + "20"+year
    
    return render_template('home.html', date= final)

if __name__ == '__main__':
    app.run(debug=True)